package vn.com.lending.otpservice.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class GatewayRequest {
    private String phoneNumber;
    private String smsText;
    private String requestor;
    private String sourceAppPassword;
    private String requestBranch;
    private String messageType;
}
