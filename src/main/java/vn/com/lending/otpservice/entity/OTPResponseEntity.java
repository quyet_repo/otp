package vn.com.lending.otpservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import org.json.JSONObject;

@Getter
@AllArgsConstructor
@Data
public class OTPResponseEntity {
    private String requestId;
    private String otp;

    @Override
    public String toString() {
        return new JSONObject(this).toString();
    }
}
