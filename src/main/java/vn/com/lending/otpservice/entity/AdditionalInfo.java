package vn.com.lending.otpservice.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AdditionalInfo {
    private String accountNumber;
    private String customerName;

    public AdditionalInfo(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
