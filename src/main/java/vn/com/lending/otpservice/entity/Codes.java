package vn.com.lending.otpservice.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Codes {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_rec")
	private Integer idRec;
	private Integer code;
	@Column(name = "request_id")
	private String requestId;
	@Column(name = "code_time")
	private LocalDateTime codeTime;
	@Column(name = "counter_regeneration")
	private Integer counterRegeneration;
	private Integer verified;
	@Column(name = "phone_number")
	private String phoneNumber;
	@Column(name = "app_id")
	private String appId;
	@Column(name="hash")
	private String hash;

	public Codes(Integer code, String requestId, LocalDateTime codeTime, String phoneNumber, String appId) {
		this.code = code;
		this.requestId = requestId;
		this.codeTime = codeTime;
		this.counterRegeneration = 0;
		this.verified = 0;
		this.phoneNumber = phoneNumber;
		this.appId = appId;
	}
}
