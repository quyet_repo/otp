package vn.com.lending.otpservice.response;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseStatus {
	@ApiModelProperty(notes = "Datetime", example = "2018-06-07T01:49:07.140+0000")
	public String timestamp = Long.toString(new Date().getTime());
	@ApiModelProperty(notes = "Status code", example = "00")
	public String status;
	@ApiModelProperty(notes = "description", example = "Success")
	public String message = "";
	@ApiModelProperty(notes = "path", example = "/security/")
	public String path = "";

	public ResponseStatus(String status, String mess) {
		this.status = status;
		this.message = mess;
	}
}
