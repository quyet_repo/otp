package vn.com.lending.otpservice.response;

import io.swagger.annotations.ApiModelProperty;

public class ErrorRs {
    @ApiModelProperty(notes = "Error datetime", example = "2018-06-07T01:49:07.140+0000")
    public int timestamp = 0;
    @ApiModelProperty(notes = "Error code", example = "400")
    public int status = 0;
    @ApiModelProperty(notes = "Error status", example = "Bad Request")
    public String error = "";
    @ApiModelProperty(notes = "Error description", example = "The limit of [3] re-generation attempts has been exceeded for requestId [0000016569fb1bd1-0a580a80011e0000]")
    public String message = "";
    @ApiModelProperty(notes = "Error path", example = "/security/otp/generate/0000016569fb1bd1-0a580a80011e0000")
    public String path = "";
}
