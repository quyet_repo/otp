package vn.com.lending.otpservice.controller;

import static vn.com.lending.otpservice.utils.Constants.APP_ID;
import static vn.com.lending.otpservice.utils.Constants.LANG;

import java.time.LocalDateTime;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import vn.com.lending.otpservice.entity.Codes;
import vn.com.lending.otpservice.entity.OTPResponseEntity;
import vn.com.lending.otpservice.exception.BadRequestException;
import vn.com.lending.otpservice.exception.NotFoundException;
import vn.com.lending.otpservice.exception.UnprocessableEntityException;
import vn.com.lending.otpservice.response.ErrorRs;
import vn.com.lending.otpservice.response.ResponseStatus;
import vn.com.lending.otpservice.service.OtpService;
import vn.com.lending.otpservice.utils.OTPHelperImpl;
import vn.com.lending.otpservice.utils.OTPRepository;
import vn.com.lending.otpservice.utils.ResourceHelper;

@RestController
@Slf4j
public class OTPController {

	@Autowired
	OTPRepository otpRepository;
	@Autowired
	OTPHelperImpl otpHelperImpl;
	@Autowired
	OtpService otpService;

	@Value("${expireTime}")
	private Integer expireTime;
	@Value("${otpRegenerationNumber}")
	private Integer OTPRegenerationNumber;

	@PutMapping(value = "/generate/{phonenumber}")
	@ResponseBody
	@ApiOperation(value = "[VIETTEL] Uses for generating/regenerating OTP.", notes = "First call - generate flow, further calls - regenerate flow")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Generate operation", response = OTPResponseEntity.class),
			@ApiResponse(code = 404, message = "Not Found Exception", response = ErrorRs.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = ErrorRs.class) })
	public OTPResponseEntity generate(@PathVariable String phonenumber, @RequestHeader String requestId) {

		Codes code = otpRepository.findByRequestId(requestId);

		if (code == null) {// generate case
//        	UUID uuid = UUID.randomUUID();
//    		String randomUUIDString = uuid.toString();
//    		LocalDateTime a = LocalDateTime.now();
//    		log.info(a.toString());
			Codes new_code = new Codes(otpHelperImpl.generateTOTP(), requestId, LocalDateTime.now(), phonenumber,
					APP_ID);
			otpRepository.save(new_code);
			log.debug("Code has been generated for request: " + new_code.getRequestId());
			otpService.sent(new_code.getPhoneNumber(), new_code.getCode().toString());
			return new OTPResponseEntity(new_code.getRequestId().toString(), new_code.getCode().toString());
		} else {// regenerate case

			if (code.getVerified() == 1) {
				throw new BadRequestException(
						String.format(ResourceHelper.getMessage(LANG, "already_verified"), code.getRequestId()));
			}

			if (code.getCounterRegeneration() > OTPRegenerationNumber) {
				throw new BadRequestException(String.format(ResourceHelper.getMessage(LANG, "limit_of_regeneration"),
						OTPRegenerationNumber, code.getRequestId()));
			}

			code.setCode(otpHelperImpl.generateTOTP());
			code.setCodeTime(LocalDateTime.now());
			code.setCounterRegeneration(code.getCounterRegeneration() + 1);
			otpRepository.save(code);
			otpService.sent(code.getPhoneNumber(), code.getCode().toString());
			log.debug("Code has been re-generated for request: " + code.getRequestId());
			return new OTPResponseEntity(code.getRequestId().toString(), code.getCode().toString());
		}
	}

	@PutMapping(value = "/verify/{requestId}")
	@ApiOperation(value = "Uses for resending of OTP process for UI page.", notes = "code from UI must present in body")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Verify operation"),
			@ApiResponse(code = 404, message = "Not Found Exception", response = ErrorRs.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = ErrorRs.class) })
	public ResponseStatus verify(@PathVariable String requestId, @RequestHeader Integer OTP) {

		Codes code = otpRepository.findByRequestId(requestId);

		if (code == null) {
			throw new NotFoundException("code_not_found" + requestId);
		}
		log.debug("Verification state of request: " + code.toString());
		if (code.getVerified() == 1) {
			// skip the flow, if request is already verified
		} else if (LocalDateTime.now().minusSeconds(expireTime).isAfter(code.getCodeTime())) {
			throw new BadRequestException("code_time_over " + expireTime + "" + code.getRequestId());
		} else if (!code.getCode().equals(OTP)) {
			throw new UnprocessableEntityException("code_not_equal " + code.getRequestId());
		} else {
			try {
				String secret = "wanMwjbIlj";
				String message = code.getCode() + code.getPhoneNumber() + code.getRequestId() + secret;
				Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
				SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
				sha256_HMAC.init(secret_key);

				String hash = Base64.encodeBase64String(sha256_HMAC.doFinal(message.getBytes()));
				code.setHash(hash);
				code.setVerified(1);
				otpRepository.save(code);
				log.debug("Request has been verified: " + code.toString());
			} catch (Exception e) {
				System.out.println("Error");
			}
		}
		return new ResponseStatus("00", "Success");
	}
}
