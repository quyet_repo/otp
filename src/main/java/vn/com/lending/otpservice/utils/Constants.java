package vn.com.lending.otpservice.utils;

public final class Constants {
    public static final String LANG = "en";
    public static final String CHANNEL = "zvay";
    public static final String APP_ID = "ZAPP";
    public static final String OPERATION = "OTP";
    public static final String MESSAGE_TYPE = "NotifyClient";
    public static final String BUNDLE = "messages";

    //URIs for iapi
    public static final String GATEWAY_URI = "/iapi/notifications/v1/sms";

    public static final String CONSENT_VERIFY_URI = "/consent/verify?requestId={requestId}";
    public static final String CONSENT_PAYLOAD_URI = "/consent/payload?requestId={requestId}";
    public static final String CONSENT_ENRICH_URI = "/consent/enrich/{requestId}";
    public static final String CONSENT_CHECK_URI = "/consent/check?requestId={requestId}";
    public static final String CONSENT_ADD_ATTEMPT_URI = "/consent/attempt/add?requestId={requestId}";
    public static final String CONSENT_CLEAR_ATTEMPT_URI = "/consent/attempt/clear?requestId={requestId}";

    public static final String TEMPLATE_WITH_LANG_URI = "/templates/generateWithParams?channel={channel}&" +
            "operation={operation}&type={type}&lang={lang}&application={application}";
    public static final String TEMPLATE_WITHOUT_LANG_URI = "/templates/generateWithParams?channel={channel}&" +
            "operation={operation}&type={type}&application={application}";
}
