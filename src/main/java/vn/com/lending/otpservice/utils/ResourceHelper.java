package vn.com.lending.otpservice.utils;

import static vn.com.lending.otpservice.utils.Constants.BUNDLE;

import java.util.Locale;
import java.util.ResourceBundle;

import org.springframework.util.StringUtils;

public class ResourceHelper {

	public static String getMessage(String lang, String key) {
		ResourceBundle resourceBundle;
		if (StringUtils.isEmpty(lang)) {
			resourceBundle = ResourceBundle.getBundle(BUNDLE);
		} else {
			resourceBundle = ResourceBundle.getBundle(BUNDLE, new Locale(lang.toLowerCase(), lang.toUpperCase()));
		}
		if (resourceBundle == null) {
			return "";
		}
		return resourceBundle.getString(key);
	}
}
