package vn.com.lending.otpservice.utils;

import org.springframework.data.jpa.repository.JpaRepository;

import vn.com.lending.otpservice.entity.Codes;

public interface OTPRepository extends JpaRepository<Codes, Integer> {

    Codes findByRequestId(String requestId);
}
