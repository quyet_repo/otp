package vn.com.lending.otpservice.utils;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;
import org.springframework.stereotype.Component;

@Component
public class OTPHelperImpl implements OTPHelper {
    /**
     * Generate unique Time-based One-time Password
     *
     * @return Integer TOTP
     */
    @Override
    public Integer generateTOTP() {
        GoogleAuthenticator gAuth = new GoogleAuthenticator();
        final GoogleAuthenticatorKey key = gAuth.createCredentials();
        String secretKey = key.getKey();
        int TOTP = gAuth.getTotpPassword(secretKey);
        if (String.valueOf(TOTP).length() == 6) {
            return TOTP;
        } else {
            return generateTOTP();
        }

    }
}
