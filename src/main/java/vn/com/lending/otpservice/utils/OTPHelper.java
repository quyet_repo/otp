package vn.com.lending.otpservice.utils;

public interface OTPHelper {
    Integer generateTOTP();
}
