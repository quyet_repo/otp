-- Dumping database structure for otpdb
CREATE DATABASE IF NOT EXISTS `otpdb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `otpdb`;

-- Dumping structure for table  otpdb.codes  
CREATE TABLE IF NOT EXISTS `codes` (
  `id_rec` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) NOT NULL,
  `request_id` char(50) NOT NULL,
  `code_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `request_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `channel` char(50) NOT NULL,
  `counter_attempts` int(1) NOT NULL,
  `counter_regeneration` int(1) NOT NULL,
  `verified` int(1) NOT NULL,
  PRIMARY KEY (`id_rec`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
