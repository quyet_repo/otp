FROM java:openjdk-8u91-jdk
CMD java ${JAVA_OPTS} -jar otpservice-0.0.2-SNAPSHOT.jar
EXPOSE 8081
COPY build/libs/otpservice-0.0.2-SNAPSHOT.jar .